(function() {
  'use strict';

  angular
    .module('zayaos')
    .factory('widgetParser', ['$log','$q', 'apiConstant', function ($log,$q, apiConstant) {
    var soundIdRegex = /(?:\[\[)(?:sound)(?:\s)(?:id=)([0-9]+)(?:\]\])/;
    var imageTagRegex = /(?:\[\[)(?:img)(?:\s)(?:id=)([0-9]+)(?:\]\])/;
    var WIDGETS = {
        'SPEAKER_IMAGE': '<div class="sound-image sbtn sbtn-sound"></div>',
        'SPEAKER_IMAGE_SELECTED': '<div class="sound-image sbtn sbtn-sound activated animation-repeat-bounce"></div>',
        'OPTIONS': {
          'LAYOUT_THRESHOLD': 55,
          'FONT_SIZE_THRESHOLD': 6
        },
        'QUESTION_TYPES': {
          'CHOICE_QUESTION': 'choicequestion',
          'SCQ': 'scq',
          'MCQ': 'mcq'
        },
        'LAYOUT': {
          'LIST': 'list',
          'GRID': 'grid'
        }
      };

    return {
      getSoundId: getSoundId,
      getImageId: getImageId,
      getImageSrc: getImageSrc,
      getSoundSrc: getSoundSrc,
      parseToDisplay: parseToDisplay,
      replaceImageTag: replaceImageTag,
      removeSoundTag: removeSoundTag,
      removeImageTag: removeImageTag,
      getLayout: getLayout,
      getOptionsFontSize: getOptionsFontSize,
      addContainerToText : addContainerToText
    }

    function getSoundId(string) {
      if (soundIdRegex.exec(string)) {
        return soundIdRegex.exec(string)[1];
      }
    }

    function getImageId(string) {
      if (imageTagRegex.exec(string)) {
        return imageTagRegex.exec(string)[1];
      }
    }

    function getImageSrc(id, index, quiz) {

      return apiConstant.serverUrl + quiz.objects[index].node.type.content.widgets.images[id];
    }
    function getSoundSrc(id, index, quiz) {
      return apiConstant.serverUrl + quiz.objects[index].node.type.content.widgets.sounds[id];
    }
    function parseToDisplay(string, index, quiz) {
      // var d = $q.defer();
      var text = removeSoundTag(string, index);
      text = addContainerToText(text)
      if (getImageId(text)) {
        var replacedText = replaceImageTag(text, index, quiz);
        return replacedText.trim().length > 0 ? replacedText.trim() : WIDGETS.SPEAKER_IMAGE;
      }
      else{
        return text.trim().length > 0 ? text.trim() : WIDGETS.SPEAKER_IMAGE
      }
    }

    function addContainerToText(text) {
        if(text.replace(imageTagRegex, "").trim().length){
            return text.match(imageTagRegex) ? "<div>" + text.replace(imageTagRegex, "") + "</div>" + text.match(imageTagRegex)[0] : "<div>" + text.replace(imageTagRegex, "") + "</div>";
        }
        else{
            return text.match(imageTagRegex) ? text.match(imageTagRegex)[0] : "";
        }
    }
    function removeSoundTag(string) {
      return string.replace(soundIdRegex, "");
    }

    function removeImageTag(string) {
      return string.replace(imageTagRegex, "");
    }

    function replaceImageTag(string, index, quiz) {
      var src = getImageSrc(getImageId(string), index, quiz);
        return string.replace(imageTagRegex, "<img class='content-image' src='" +
          src + "'>");
    }

    function getLayout(question, index, quiz) {
      var layout = WIDGETS.LAYOUT.LIST;

      angular.forEach(question.node.type.content.options, function(option) {
        if (getImageId(option.option) || getSoundId(option.option)) {
          layout = WIDGETS.LAYOUT.GRID;
        }
        // var text = removeImageTag(removeSoundTag(option.option));
        // text = text.trim();
        // if (text.length >= WIDGETS.OPTIONS.LAYOUT_THRESHOLD) {
        //   layout =  WIDGETS.LAYOUT.LIST;
        // }
      }, this);
      return layout;
    }

    function getOptionsFontSize(options) {
      var size = 'font-lg'
      angular.forEach(options, function(option) {
        if (option.widgetHtml.length > WIDGETS.OPTIONS.FONT_SIZE_THRESHOLD) {
          size = 'font-md'
        }
      })
      return size;
    }
  }]);

})();
