(function() {
    angular.module('zayaos')
        .filter('trusted', function($sce) {
            return function(url) {
                return $sce.trustAsResourceUrl(url);
            };
        });
})();
