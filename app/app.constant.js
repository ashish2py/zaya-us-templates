(function(){
	angular.module('zayaos.constant', [])
		.constant("apiConstant",{
			"serverUrl" : "http://metalearn-api.zaya.in/",
			"apiBase": "api/v1/",
			"apiAuthBase" : "rest-auth/",
			"apiLogin": "login/",
			"apiLogout": "logout/",
			"apiRegistration": "registration/",
			"apiProfile": "profiles/",
			"apiUser": 'user/',
			"apiAccount": 'accounts/',

			"apiPost": 'posts/',
			"apiBragtag": "bragtags/",
			"apiBragtagPack": "bragtag-packs/",

			"apiCourses": "courses",
			"apiNode": "nodes",

			"apiPlaylist": "playlist"
		});	
})();
