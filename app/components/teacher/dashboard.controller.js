(function() {
    angular.module('zayaos.teacher')
        .controller('teacherDashboardController', [ "$stateParams", '$scope',function($stateParams, $scope){
        	var ctrl = this;

        ctrl.sparkline_1 = {data:null, opts:null}
        ctrl.sparkline_1.data = [0,5,3,7,5,10,3,6,5,10];
        ctrl.sparkline_1.opts = { 
                                    type:'line',
                                    width: '85',
                                    height: '35',
                                    lineColor: 'black',
                                    highlightSpotColor: 'red',
                                    highlightLineColor: 'blue',
                                    fillColor: false,
                                    spotColor: false,
                                    minSpotColor: false,
                                    maxSpotColor: false,
                                    lineWidth: 1.15
                                  };

        ctrl.sparkline_2 = {data:null, opts:null}
        ctrl.sparkline_2.data = [5,8,7,10,9,10,8,6,4,6,8,7,6,8];
        ctrl.sparkline_2.opts = { 
                                  type: 'bar', 
                                  width: '85',
                                  height: '35',
                                  barWidth: 3,
                                  barSpacing: 3,
                                  chartRangeMin: 0,
                                  barColor: 'red' 
                                };

        ctrl.sparkline_3 = {data:null, opts:null}
        ctrl.sparkline_3.data = [5,8,7,10,9,10,8,6,4,6,8,7,6,8];
        ctrl.sparkline_3.opts = { 
                                  type: 'discrete', 
                                  width: '85',
                                  height: '35',
                                  lineHeight: 20,
                                  lineColor: 'green',
                                  xwidth: 18 
                                };

        ctrl.sparkline_4 = {data:null, opts:null}
        ctrl.sparkline_4.data = [2,5,3,7,5,10,3,6,5,7];
        ctrl.sparkline_4.opts = { 
                                      width: '85',
                                      height: '35',
                                      lineColor: 'red',
                                      highlightSpotColor: 'black',
                                      highlightLineColor: 'red',
                                      fillColor: false,
                                      spotColor: false,
                                      minSpotColor: false,
                                      maxSpotColor: false,
                                      lineWidth: 1.15
                                    };

          ctrl.tabs = [{
                percent:90,
                icon:'bike',
                subject:"5th Grade Section A",
                goals:[{
                    title:"5A",
                    steps:[ 
                            'Make 1 video to experiment with "Flipping my Classroom"',
                            'Create targeted intervention for my tier 3 students',
                            'Apply for masters program by December '
                        ]
                }],
                objectives:[{
                    title:"ELA",
                    progress:40,
                    total:100,
                },
                {
                    title:"Math",
                    progress:50,
                    total:100,
                },
                {
                    title:"Health & Wellness",
                    progress:75,
                    total:100,
                },
                {
                    title:"SEL",
                    progress:25,
                    total:100,
                }],
                professionalDev:[
                    {
                      title:"Flipped Classroom",
                      video:"https://www.youtube.com/embed/9aGuLuipTwg"
                    },
                    {
                      title:"Equivalent Fractions",
                      video:"https://www.youtube.com/embed/DAyG_Y-u3Ts"
                    }
                ]
            },{
                percent:60,
                icon:'car',
                subject:"Section B",
                goals:[{
                    title:"5B",
                    steps:[ 
                            'Make 1 video to experiment with "Flipping my Classroom"',
                            'Create targeted intervention for my tier 3 students',
                            'Apply for masters program by December '
                        ]
                    }],
                objectives:[{
                    title:"ELA",
                    progress:60,
                    total:100,
                },
                {
                    title:"Math",
                    progress:80,
                    total:100,
                },
                {
                    title:"Health & Wellness",
                    progress:40,
                    total:100,
                },
                {
                    title:"SEL",
                    progress:60 ,
                    total:100,

                }],
                professionalDev:[
                    {
                      title:"Flipped Classroom",
                      video:"https://www.youtube.com/embed/9aGuLuipTwg"
                    },
                    {
                      title:"Equivalent Fractions",
                      video:"https://www.youtube.com/embed/DAyG_Y-u3Ts"
                    }
                ]
                
            }]

            ctrl.loadTab = function(tab)
            {
                if (ctrl.isActive==undefined || ctrl.isActive != tab)
                {
                    ctrl.isActive = tab;
                    $scope.tabContent  = ctrl.tabs[ctrl.isActive];
                }
            }
            ctrl.loadTab(0);
        }]);
})();
