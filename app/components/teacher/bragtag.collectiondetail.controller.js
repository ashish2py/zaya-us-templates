(function() {
    angular.module('zayaos.teacher')
        .controller('teacherBragtagCollectionDetailController', ['bragtags','authentication', '$stateParams', function(bragtags, authentication, $stateParams) {
            var ctrl = this;
            ctrl.bragtags = null;
            ctrl.collection  = null;

            bragtags.getBragtagCollectionDetail($stateParams.collectionId).then(function(collection){
                ctrl.collection = collection;
                ctrl.bragtags = collection.bragtags;                
            }, function(error){
            	console.log(error);
            })

            ctrl.removeBragtagFromCollection = function(bragtag){
                var collectionBragtag = [];
                for (var i = 0; i < ctrl.collection.bragtags.length; i++) {
                    collectionBragtag.push(ctrl.collection.bragtags[i].id);
                }
                if (collectionBragtag.indexOf(bragtag.id) != -1) {
                    collectionBragtag.splice(collectionBragtag.indexOf(bragtag.id),1);
                    var data = {
                        "title": ctrl.collection.title,
                        "description": ctrl.collection.description,
                        "bragtags": collectionBragtag
                    }
                    bragtags.updateBragTagCollection(ctrl.collection.id, data).then(function(data) {
                        ctrl.bragtags.splice(ctrl.bragtags.indexOf(bragtag),1);
                    }, 
                    function(error) {
                        console.log(error);
                    })
                }
            }

        }]);
})();
