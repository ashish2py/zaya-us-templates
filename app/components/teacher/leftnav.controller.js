(function() {
    angular.module('zayaos.teacher')
        .controller('teacherLeftnavController', [ "$scope", "$location", 'bragtags',function($scope, $location, bragtags){
        	var ctrl = this;
        	bragtags.getBragtagCollections().then(function(collections){
        		ctrl.collections = collections;
        	}, function(error){
        		console.log(error);
        	});

        	$scope.isActive = function (viewLocation) { 
                return viewLocation === $location.path();
            };
        }]);
})();
 