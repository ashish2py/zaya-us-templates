(function() {
    angular.module('zayaos.teacher')
        .controller('teacherBragtagCollectionController', ['bragtags','authentication', function(bragtags, authentication) {
            var ctrl = this;
            ctrl.collections = null;
            ctrl.user = authentication.getUser();

            bragtags.getBragtagCollections().then(function(collections){
            	ctrl.collections = collections;
            }, function(error){
            	console.log(error);
            })
        }]);
})();
