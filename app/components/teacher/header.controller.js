(function() {
    angular.module('zayaos.teacher')
        .controller('teacherHeaderController', [ 'authentication', '$state', function(authentication, $state){
        	const LOGIN_STATE = "auth.login";

        	var ctrl = this;
            ctrl.user = authentication.getUser();

            ctrl.getTitle = function(){
                return $state.current.stateParams.dashboardTitle;
            }
        	ctrl.logout = function(){
        		authentication.logout()
        			.then(function(){
		        		$state.go(LOGIN_STATE);
        			}, function(error){
        				console.log(errors);
        			});
        	}
        }]);
})();
