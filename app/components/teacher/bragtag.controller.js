(function() {
    angular.module('zayaos.teacher')
        .controller('teacherBragtagController', ['bragtags', 'authentication', '$state', '$q', function(bragtags, authentication, $stateParams, $q) {
            var ctrl = this;
            ctrl.bragtags = null;
            ctrl.newBragtag = {
                "title": "",
                "description": "",
                "image": ""
            }
            ctrl.user = authentication.getUser();
            ctrl.posts = [];
            ctrl.students = [{
                "profileId": "69194904-3b09-4c97-b311-f7a104692a38",
                "name": "Carlos Herrera",
                "avatar": "/app/assets/img/carlos-2.jpg"
            }, {
                "profileId": "63f80f24-f335-46db-a23f-fbf45b90753c",
                "name": "Amy Chang",
                "avatar": "/app/assets/img/amy-2.jpg"
            }, {
                "profileId": "59b84335-4471-47e6-8e11-171cab004c34",
                "name": "Jamal Barnes",
                "avatar": "/app/assets/img/jamal-2.jpg"
            }, {
                "profileId": "1cc29fe7-18c5-477e-a376-7286d470bb53",
                "name": "Alexa Jackson",
                "avatar": "/app/assets/img/alexa-2.jpg"
            }]

            //Get bragtags count
            var promisse = [];
            var savePosts = function(index, profileId) {
                return bragtags.getPostsForId(profileId).then(function(posts){
                    ctrl.students[index]["posts"] = posts;
                });
            }
            for (var i = 0; i < ctrl.students.length; i++) {
                promisse.push(savePosts(i, ctrl.students[i].profileId));
            }

            $q.all(promisse).then(function(data) {
                for (var i = 0; i < ctrl.students.length; i++) {
                    var kudos = 0;
                    var nudges = 0;
                    var last_badge = null;
                    for (var j = 0; j < ctrl.students[i].posts.length; j++) {
                        for (l = 0; l < ctrl.students[i].posts[j].brags.length; l++) {

                            for (var k = 0; k < ctrl.students[i].posts[j].brags[l].bragtag.category.length; k++) {
                        
                                if (ctrl.students[i].posts[j].brags[l].bragtag.category[k].title == "Kudos") {
                                    kudos++;
                        
                                    if (last_badge == null) {
                                        last_badge = "Kudos";
                                    }
                        
                                } else if (ctrl.students[i].posts[j].brags[l].bragtag.category[k].title == "Nudges") {
                                    nudges++;
                                    if (last_badge == null) {
                                        last_badge = "Nudges";
                                    }
                                }
                            }
                        }

                    }
                    ctrl.students[i]["last_badge"] = last_badge==null?"None":last_badge;
                    ctrl.students[i]["kudos"] = kudos;
                    ctrl.students[i]["nudges"] = nudges;
                }

            });

            // Utils
            var updateBragtags = function() {
                //Get bragtags
                bragtags.getBragTags().then(function(bragtags) {
                    ctrl.bragtags = bragtags;
                }, function(error) {
                    console.log(error);
                });
            }

            bragtags.getBragtagCollections().then(function(collections) {
                ctrl.collections = collections;
            }, function(error) {
                console.log(error);
            });


            //Controller func
            ctrl.addBragTag = function() {
                bragtags.createBragTags(ctrl.newBragtag).then(function(data) {
                    updateBragtags();
                }, function(error) {
                    console.log(error);
                });
            }
            ctrl.deleteBragtag = function(bragtag) {
                bragtags.deleteBragTags(bragtag).then(function(data) {
                    ctrl.bragtags.splice(ctrl.bragtags.indexOf(bragtag), 1);
                }, function(error) {
                    console.log(error);
                });
            }
            ctrl.addBragtagToCollection = function(bragtag, collection) {
                var collectionBragtag = [];
                for (var i = 0; i < collection.bragtags.length; i++) {
                    collectionBragtag.push(collection.bragtags[i].id);
                }
                if (collectionBragtag.indexOf(bragtag.id) == -1) {
                    collectionBragtag.push(bragtag.id);
                    var data = {
                        "title": collection.title,
                        "description": collection.description,
                        "bragtags": collectionBragtag
                    }
                    bragtags.updateBragTagCollection(collection.id, data).then(function(data) {

                        },
                        function(error) {
                            console.log(error);
                        })
                }
            }

            bragtags.getPosts().then(function(posts) {
                var postSet = new Set();

                var type = $state.current.stateParams.type
                ctrl.type = type;

                for (var i = 0; i < posts.length; i++) {
                    var category = posts[i].brags[0].bragtag.category
                    if (category.length < 1) {
                        console.log('erro');
                    } else {
                        for (var j = 0; j < category.length; j++) {
                            if (category[j].title == "Kudos" && type == "kudos") {
                                postSet.add(posts[i]);
                            } else if (category[j].title == "Nudges" && type == "nudges") {
                                postSet.add(posts[i]);
                            }
                        }
                    }
                }

                ctrl.posts = Array.from(postSet);

            }, function(error) {
                console.log(error);
            });



            //Update UI
            updateBragtags();
        }])
})();
