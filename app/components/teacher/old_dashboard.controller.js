(function() {
    angular.module('zayaos.teacher')
        .controller('teacherOldDashboardController', [ "$stateParams", function($stateParams){
        	var ctrl = this;
            ctrl.sparkline_1 = {data:null, opts:null}
            ctrl.sparkline_1.data = [0,5,3,7,5,10,3,6,5,10];
            ctrl.sparkline_1.opts = { 
                                        type:'line',
                                        width: '85',
                                        height: '35',
                                        lineColor: 'black',
                                        highlightSpotColor: 'red',
                                        highlightLineColor: 'blue',
                                        fillColor: false,
                                        spotColor: false,
                                        minSpotColor: false,
                                        maxSpotColor: false,
                                        lineWidth: 1.15
                                      };

            ctrl.sparkline_2 = {data:null, opts:null}
            ctrl.sparkline_2.data = [5,8,7,10,9,10,8,6,4,6,8,7,6,8];
            ctrl.sparkline_2.opts = { 
                                      type: 'bar', 
                                      width: '85',
                                      height: '35',
                                      barWidth: 3,
                                      barSpacing: 3,
                                      chartRangeMin: 0,
                                      barColor: 'red' 
                                    };

            ctrl.sparkline_3 = {data:null, opts:null}
            ctrl.sparkline_3.data = [5,8,7,10,9,10,8,6,4,6,8,7,6,8];
            ctrl.sparkline_3.opts = { 
                                      type: 'discrete', 
                                      width: '85',
                                      height: '35',
                                      lineHeight: 20,
                                      lineColor: 'green',
                                      xwidth: 18 
                                    };

            ctrl.sparkline_4 = {data:null, opts:null}
            ctrl.sparkline_4.data = [2,5,3,7,5,10,3,6,5,7];
            ctrl.sparkline_4.opts = { 
                                          width: '85',
                                          height: '35',
                                          lineColor: 'red',
                                          highlightSpotColor: 'black',
                                          highlightLineColor: 'red',
                                          fillColor: false,
                                          spotColor: false,
                                          minSpotColor: false,
                                          maxSpotColor: false,
                                          lineWidth: 1.15
                                        };

            ctrl.counters = [1200, 2300, 2330, 2343];
            ctrl.mainchart = {
              data:[
                    {
                      data: [
                              [1, 35],
                              [2, 60],
                              [3, 40],
                              [4, 65],
                              [5, 45],
                              [6, 75],
                              [7, 35],
                              [8, 40],
                              [9, 60]
                            ], 
                      canvasRender: true
                    },
                    {
                      data: [
                              [1, 20],
                              [2, 40],
                              [3, 25],
                              [4, 45],
                              [5, 25],
                              [6, 50],
                              [7, 35],
                              [8, 60],
                              [9, 30]
                            ], 
                      canvasRender: true
                    },
                    {
                      data: [
                              [1, 35],
                              [2, 15],
                              [3, 20],
                              [4, 30],
                              [5, 15],
                              [6, 18],
                              [7, 28],
                              [8, 10],
                              [9, 30]
                            ], 
                      canvasRender: true
                    }
                ],
              opts:{
                series: {
                  lines: {
                    show: true,
                    lineWidth: 0, 
                    fill: true,
                    fillColor: { colors: [{ opacity: 1 }, { opacity: 1 }] }
                  },
                  fillColor: "rgba(0, 0, 0, 1)",
                  shadowSize: 0,
                  curvedLines: {
                    apply: true,
                    active: true,
                    monotonicFit: true
                  }
                },
                legend:{
                  show: false
                },
                grid: {
                  show: true,
                  margin: {
                    top: 20,
                    bottom: 0,
                    left: 0,
                    right: 0,
                  },
                  labelMargin: 0,
                  minBorderMargin: 0,
                  axisMargin: 0,
                  tickColor: "rgba(0,0,0,0.05)",
                  borderWidth: 0,
                  hoverable: true,
                  clickable: true
                },
                colors: ['rgb(66, 133, 244)', 'rgb(129, 173, 248)', 'rgb(162, 195, 250)'],
                xaxis: {
                  tickFormatter: function(){
                    return '';
                  },
                  autoscaleMargin: 0,
                  ticks: 11,
                  tickDecimals: 0,
                  tickLength: 0
                },
                yaxis: {
                  tickFormatter: function(){
                    return '';
                  },
                  //autoscaleMargin: 0.01,
                  ticks: 4,
                  tickDecimals: 0
                }
              }
            }
            ctrl.piechart = {
              data:[
                { label: "Services", data: 33 },
                { label: "Standard Plans", data: 33 },
                { label: "Services", data: 33 }
              ],
              opts:{
                series: {
                  pie: {
                    radius: 0.75,
                    innerRadius: 0.58,
                    show: true,
                    highlight: {
                      opacity: 0.1
                    },
                    label: {
                      show: false
                    }
                  }
                },
                grid:{
                  hoverable: true,
                },
                legend:{
                  show: false
                },
                colors: ['rgb(52, 168, 83)', 'rgb(251, 188, 5)', 'rgb(66, 133, 244)']
              }
            }
            ctrl.lists = ["MATH", "SEL"];
            ctrl.cards = ["Course 1", "Course 2", "Course 3","Course 4"];

        }]);
})();
