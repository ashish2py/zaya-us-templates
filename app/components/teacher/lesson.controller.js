(function(){
	angular.module('zayaos.teacher')
		.controller('teacherLessonController',['$stateParams','content','authentication' ,function($stateParams, content, authentication){
			var ctrl = this;
			ctrl.lesson = {};

			content.getNodeDetail($stateParams.lessonId).then(function(data){
				ctrl.lesson = data;
				console.log(data);

			}, function(error){
				console.log(error);
			})

			ctrl.setLessonActive = function(indexActive)
			{	
				ctrl.activeIndex = indexActive;
			}
			ctrl.setLessonActive(0);


			ctrl.getVideoUrl = function(url){
				return 'http://metalearn-api.zaya.in'+url;
			}

			
		}]);
})();
