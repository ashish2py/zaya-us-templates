(function() {
    angular.module('zayaos.teacher')
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider.state('teacher', {
                    url: "/teacher",
                    abstract: "true",
                    views: {
                        '': {
                            templateUrl: 'app/components/teacher/templates/base.html',
                        },
                        'header@teacher': {
                            templateUrl: 'app/components/teacher/templates/header.html',
                            controller: "teacherHeaderController as ctrl"
                        },
                        'leftnav@teacher': {
                            templateUrl: 'app/components/teacher/templates/leftnav.html',
                            controller: "teacherLeftnavController as ctrl"
                        },
                        'rightnav@teacher': {
                            templateUrl: 'app/components/teacher/templates/rightnav.html',
                            controller: "teacherRightnavController as ctrl"
                        }
                    }
                })
                // dashboard
                .state('teacher.dashboard', {
                    url: "/dashboard",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Dashboard"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/dashboard.html',
                            controller: "teacherDashboardController as ctrl"
                        }
                    }
                })
                // Classes
                .state('teacher.classes', {
                    url: "/classes",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Classes"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/classes.html',
                        }
                    }
                })
                // Class 1
                .state('teacher.class1', {
                    url: "/class/grade-5",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Class 5A"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/classes.html',
                            controller: "teacherClassesController as ctrl"
                        }
                    }
                })
                // Class 2
                .state('teacher.class2', {
                    url: "/class2",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Class 2"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/classes.html',
                        }
                    }
                })
                // Need Attention
                .state('teacher.attention', {
                    url: "/attention",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Groups"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/classes.html',
                        }
                    }
                })
                // On Track
                .state('teacher.ontrack', {
                    url: "/ontrack",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Groups"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/classes.html',
                        }
                    }
                })
                // Exceeding
                .state('teacher.exceeding', {
                    url: "/exceeding",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Groups"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/classes.html',
                        }
                    }
                })
                // Knowlegemap grade 1
                .state('teacher.knowledgemap1', {
                    url: "/knowledgemap/grade-1",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Knowlege Maps"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/kmap_grade1.html',
                            controller: "teacherKmapController as ctrl"
                        }
                    }
                })
                // Cource Math 5
                .state('teacher.course1', {
                    url: "/course/english",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "English"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/course1.html',
                            controller: "teacherDashboardController as ctrl"
                        }
                    }
                })
                .state('teacher.course2', {
                    url: "/course/math",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Math"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/course2.html',
                            controller: "teacherDashboardController as ctrl"
                        }
                    }
                })
                .state('teacher.lessondetail', {
                    url: "/lesson/:lessonId",
                    data:{
                        "isAuthenticated": true,
                        "authRedirect": "auth.login" 
                    },
                    stateParams: {
                        "dashboardTitle": "Playlist"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/lesson.html',
                            controller: "teacherLessonController as ctrl"
                        }
                    }
                })
                .state('teacher.emails', {
                    url: "/emails",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Messages"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/emails.html',
                        }
                    }
                })
                .state('teacher.emaildetail', {
                    url: "/emaildetail",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Messages"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/email_details.html',
                        }
                    }
                })
                .state('teacher.sms', {
                    url: "/sms",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Messages"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/sms.html',
                        }
                    }
                })
                .state('teacher.smsdetail', {
                    url: "/smsdetail",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Messages"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/sms_details.html',
                        }
                    }
                })
                .state('teacher.reports', {
                    url: "/reports",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Report"
                    },
                    views: {
                        'body@teacher': {
                            template: "<h1 class='text-center'>Coming Soon..</h1>",
                        }
                    }
                })

                .state('teacher.bragtagsActivity', {
                    url: "/bragtags",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Kudos/Nudges"
                    },
                    abstract : true
                })

                .state('teacher.bragtagsActivity.kudos', {
                    url: "/kudos",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Kudos",
                        "type": "kudos"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/bragtags-activity.html',
                            controller: 'teacherBragtagController as ctrl'
                        }
                    }
                })
                
                .state('teacher.bragtagsActivity.activity', {
                    url: "/activity",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Kudos/Nudges",
                        "type": "nudges"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/bragtags-activity.html',
                            controller: 'teacherBragtagController as ctrl'
                        }
                    }
                })

                .state('teacher.bragtagcollection', {
                    url: "/bragtagcollection",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "BragTags Collection"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/bragtag_collection.html',
                            controller: 'teacherBragtagCollectionController as ctrl'
                        }
                    }
                })
                .state('teacher.bragtagcollectionDetails', {
                    url: "/bragtagcollection/:collectionId",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "BragTags"
                    },
                    views: {
                        'body@teacher': {
                            templateUrl: 'app/components/teacher/templates/bragtag_collection_detail.html',
                            controller: 'teacherBragtagCollectionDetailController as ctrl'
                        }
                    }
                });
        }])
})();
