(function() {
    angular.module('zayaos.teacher')
        .controller('teacherKmapController', ["$stateParams", function($stateParams) {
            var ctrl = this;
            ctrl.lessons = kmap;
            
            ctrl.lessonQuery=null;
            ctrl.selectedLesson = null;
            ctrl.selectedStudent = null;
            ctrl.selectedPlaylist = null;           

            ctrl.viewTree = function(lesson){
              ctrl.selectedLesson = lesson;
              ctrl.lessonQuery=null;
              
              ctrl.lesson_unit = lesson.unit
              ctrl.lesson_topic = lesson.topic

              ctrl.playlist = viewTree(lesson.sr);
              console.log(ctrl.selectedLesson.sr)
              console.log('ctrl.playlist', ctrl.playlist);
              ctrl.studentScores = studentScores;
            }

            ctrl.viewUserTree = function(){
              ctrl.playlist = viewTree(ctrl.selectedLesson.sr, ctrl.selectedStudent[0]);
              ctrl.selectedPlaylist = ctrl.playlist;
              console.log(ctrl.selectedLesson.sr)
              console.log(ctrl.playlist);
            }

            ctrl.viewTree(kmap[2]);
            
        }]);
})();
