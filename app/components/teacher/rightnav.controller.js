(function() {
    angular.module('zayaos.teacher')
        .controller('teacherRightnavController', ['bragtags', 'authentication', function(bragtags, authentication) {
            var ctrl = this;
            ctrl.bragtags = null;
            ctrl.posts = null;
            ctrl.userList = authentication.dummyUserList;
            ctrl.chatUser = null;
            ctrl.chatMessages = [{
                "msgType": "chat",
                "type": "other",
                "message": "Hello",
            }, {
                "msgType": "chat",
                "type": "self",
                "message": "Hi, how are you?",
            }, {
                "msgType": "chat",
                "type": "other",
                "message": "Good, You a did a great job.",
            }, {
                "msgType": "chat",
                "type": "self",
                "message": "Thanks!",
            }, ]

            ctrl.addChatMessage = function(message){
                ctrl.chatMessages.push({
                    "type": "self",
                    "msgType": "chat",
                    "message": message
                });
            }
            ctrl.posttag = function(bragtag){
                bragtags.createPost({
                    "title": bragtag.title,
                    "bragtags": [bragtag.id],
                    "tag_to": [ctrl.chatUser.profile_id]
                })
                ctrl.chatMessages.push({
                    "type": "self",
                    "msgType": "post",
                    "post": {
                        "image": bragtag.image,
                    }
                })
            }
            ctrl.setChatUser = function(user){
                ctrl.chatUser = user;
                bragtags.getPostsForId(user.profile_id).then(function(posts){
                    if(posts[0]){
                        ctrl.chatMessages.push({
                            "msgType": "post",
                            "type": 'self',
                            "post": posts[0]
                        })
                    }
                })
            }

            bragtags.getBragTags().then(function(bragtagsCollection) {
                ctrl.bragtags = bragtagsCollection;
            }, function(error) {
                console.log(error);
            });

            bragtags.getPosts().then(function(posts) {
                ctrl.posts = posts;
            }, function(error) {
                console.log(error);
            });

        }]);
})();
