(function() {
    angular.module('zayaos.student')
        .controller('studentFitnessActivityController', [ "$stateParams", function($stateParams){
        	var ctrl = this;
            ctrl.sparkline_1 = {data:null, opts:null}
            ctrl.sparkline_1.data = [0,5,3,7,5,10,3,6,5,10];
            ctrl.sparkline_1.opts = { 
                                        type:'line',
                                        width: '85',
                                        height: '35',
                                        lineColor: 'black',
                                        highlightSpotColor: 'red',
                                        highlightLineColor: 'blue',
                                        fillColor: false,
                                        spotColor: false,
                                        minSpotColor: false,
                                        maxSpotColor: false,
                                        lineWidth: 1.15
                                      };

            ctrl.sparkline_2 = {data:null, opts:null}
            ctrl.sparkline_2.data = [5,8,7,10,9,10,8,6,4,6,8,7,6,8];
            ctrl.sparkline_2.opts = { 
                                      type: 'bar', 
                                      width: '85',
                                      height: '35',
                                      barWidth: 3,
                                      barSpacing: 3,
                                      chartRangeMin: 0,
                                      barColor: 'red' 
                                    };

            ctrl.sparkline_3 = {data:null, opts:null}
            ctrl.sparkline_3.data = [5,8,7,10,9,10,8,6,4,6,8,7,6,8];
            ctrl.sparkline_3.opts = { 
                                      type: 'discrete', 
                                      width: '85',
                                      height: '35',
                                      lineHeight: 20,
                                      lineColor: 'green',
                                      xwidth: 18 
                                    };

            ctrl.sparkline_4 = {data:null, opts:null}
            ctrl.sparkline_4.data = [2,5,3,7,5,10,3,6,5,7];
            ctrl.sparkline_4.opts = { 
                                          width: '85',
                                          height: '35',
                                          lineColor: 'red',
                                          highlightSpotColor: 'black',
                                          highlightLineColor: 'red',
                                          fillColor: false,
                                          spotColor: false,
                                          minSpotColor: false,
                                          maxSpotColor: false,
                                          lineWidth: 1.15
                                        };

            ctrl.counters = [1200, 2300, 2330, 2343];


        }]);
})();
