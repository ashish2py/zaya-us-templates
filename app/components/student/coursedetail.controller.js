(function() {
    angular.module('zayaos.student')
        .controller('studentCourseDetailController', ['authentication', 'content', '$stateParams', '$state', function(authentication, content, $stateParams, $state) {
            const lessonDetailState = 'student.lessondetail';
            var ctrl = this;
            ctrl.user = authentication.getUser();
            ctrl.content = [{
                "title": "",
                "lessons": []
            }];

            content.getNodeDetail($stateParams.courseId).then(function(data) {
                ctrl.content[0].title = data.node.title;
                ctrl.content[0].lessons = data.objects;

                //If lesson is english add ml content
                if(ctrl.content[0].title.toLowerCase().trim() == "english"){
                	var mlplaylist = JSON.parse(localStorage.coursePlaylist);
                    var nodeidlist = [];
                    var duplist = [];
                    for (var i = 0; i < mlplaylist.length; i++) {
                        if(nodeidlist.indexOf(mlplaylist[i].sr)!=-1){
                            duplist.push(mlplaylist[i].sr);    
                        }
                        nodeidlist.push(mlplaylist[i].sr);
                    }
                	ctrl.content[0].lessons = []
		            content.getNodeList(nodeidlist).then(function(playlist){
                        for (var i = 0; i < playlist.length; i++) {
                            var lessonId = playlist[i].id;
                            var index = nodeidlist.indexOf(lessonId);
		            		ctrl.content[0].lessons[index] = { 
                                "node": playlist[i],
                                "mldata": mlplaylist[index]
                             };
                             nodeidlist[index] = undefined;
                             if(duplist.indexOf(lessonId)!=-1){
                                duplist[duplist.indexOf(lessonId)] = undefined;
                                playlist.push(playlist[i]);
                             }
                        }
                        console.log(ctrl.content[0].lessons);
		            }, function(error){
		            	console.log(error);
		            });
                }

            }, function(error) {
                console.log(error);
            })

            ctrl.showLesson = function(lessonid, index){
                $state.go(lessonDetailState,{'lessonId': lessonid});
                selectedPlaylist = index;
            }


        }]);
})();
