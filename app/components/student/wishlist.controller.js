(function() {
    angular.module('zayaos.student')
        .controller('studentWishlistController', [ 'authentication', function(authentication) {
            var ctrl = this;
            ctrl.user = authentication.getUser();
            ctrl.wishlist = [
                {
                    "date": new Date(2016,10,1),
                    "title": "Goals",
                    "todo":[
                        {
                            "title": "15 books",
                            "done": false,
                        },
                        {
                            "title": "3 cycles",
                            "done": false,
                        },
                        {
                            "title": "Learn Guitar",
                            "done": true,
                        }
                    ]
                }
            ];

            ctrl.addToWishlist = function(wishlist){
                wishlist["date"] = new Date();
                var copyObj = {};
                angular.copy(wishlist, copyObj);
                ctrl.wishlist.push(copyObj);
            }
        }]);
})();
