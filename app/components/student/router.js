(function() {
    angular.module('zayaos.student')
        .config(['$stateProvider', function($stateProvider) {
            // dashboard
            $stateProvider.state('student', {
                    url:"/student",
                    abstract: "true",
                    views: {
                        '': {
                            templateUrl: 'app/components/student/templates/base.html',
                        },
                        'header@student': {
                            templateUrl: 'app/components/student/templates/header.html',
                            controller: "studentHeaderController as ctrl"
                        },
                        'leftnav@student': {
                            templateUrl: 'app/components/student/templates/leftnav.html',
                            controller: "studentLeftnavController as ctrl",

                        },
                        'rightnav@student': {
                            templateUrl: 'app/components/student/templates/rightnav.html',
                            controller: "studentRightnavController as ctrl"

                        }

                    }
                })
                .state('student.dashboard', {
                    url: "/dashboard",
                    data:{
                        "isAuthenticated": true,
                        "authRedirect": "auth.login" 
                    },
                    stateParams: {
                        "dashboardTitle": "Dashboard"
                    },
                    views: {
                        'body@student': {
                            templateUrl: 'app/components/student/templates/dashboard.html',
                            controller: "studentDashboardController as ctrl"
                        }
                    }
                })
                //Manage Profile
                .state('student.profile', {
                    url: "/profile",
                    data:{
                        "isAuthenticated": true,
                        "authRedirect": "auth.login" 
                    },
                    stateParams: {
                        "dashboardTitle": "Profile"
                    },
                    views: {
                        'body@student': {
                            templateUrl: 'app/components/student/templates/profile.html',
                            controller: "studentProfileController as ctrl"
                        }
                    }
                })
                .state('student.coursedetail', {
                    url: "/course/:courseId",
                    data:{
                        "isAuthenticated": true,
                        "authRedirect": "auth.login" 
                    },
                    stateParams: {
                        "dashboardTitle": "Playlist"
                    },
                    views: {
                        'body@student': {
                            templateUrl: 'app/components/student/templates/course_detail.html',
                            controller: "studentCourseDetailController as ctrl"
                        }
                    }
                })
                .state('student.lessondetail', {
                    url: "/lesson/:lessonId",
                    data:{
                        "isAuthenticated": true,
                        "authRedirect": "auth.login" 
                    },
                    stateParams: {
                        "dashboardTitle": "Playlist"
                    },
                    views: {
                        'body@student': {
                            templateUrl: 'app/components/student/templates/lesson.html',
                            controller: "studentLessonController as ctrl"
                        }
                    }
                })
                //Manage playlist
                .state('student.playlist1', {
                    url: "/playlist1",
                    data:{
                        "isAuthenticated": true,
                        "authRedirect": "auth.login" 
                    },
                    stateParams: {
                        "dashboardTitle": "Playlist"
                    },
                    views: {
                        'body@student': {
                            templateUrl: 'app/components/student/templates/playlist.html',
                            controller: "studentPlaylistController as ctrl"
                        }
                    }
                })
                .state('student.lesson', {
                    url: "/lesson",
                    data:{
                        "isAuthenticated": true,
                        "authRedirect": "auth.login" 
                    },
                    stateParams: {
                        "dashboardTitle": "Lesson"
                    },
                    views: {
                        'body@student': {
                            templateUrl: 'app/components/student/templates/lesson.html',
                            controller: "studentLessonController as ctrl"
                        }
                    }
                })
                .state('student.playlist2', {
                    url: "/playlist2",
                    data:{
                        "isAuthenticated": true,
                        "authRedirect": "auth.login" 
                    },
                    stateParams: {
                        "dashboardTitle": "Playlist"
                    },
                    views: {
                        'body@student': {
                            templateUrl: 'app/components/student/templates/playlist.html',
                            controller: "studentPlaylistController as ctrl"
                        }
                    }
                })
                //Wishlist
                .state('student.goals', {
                    url: "/goals",
                    data:{
                        "isAuthenticated": true,
                        "authRedirect": "auth.login" 
                    },
                    stateParams: {
                        "dashboardTitle": "Goals"
                    },
                    views: {
                        'body@student': {
                            templateUrl: 'app/components/student/templates/wishlist.html',
                            controller: "studentWishlistController as ctrl"
                        },
                    }
                })
                 //Setup Gear
                .state('student.setupgear', {
                    url: "/setupgear",
                    data:{
                        "isAuthenticated": true,
                        "authRedirect": "auth.login" 
                    },
                    stateParams: {
                        "dashboardTitle": "Setup Gear"
                    },
                    views: {
                        'body@student': {
                            templateUrl: 'app/components/student/templates/setup_gear.html',
                        }
                    }
                })
                 //Activity
                .state('student.activity', {
                    url: "/activity",
                    data:{
                        "isAuthenticated": true,
                        "authRedirect": "auth.login" 
                    },
                    stateParams: {
                        "dashboardTitle": "Activity"
                    },
                    views: {
                        'body@student': {
                            templateUrl: 'app/components/student/templates/activity.html',
                            controller: "studentFitnessActivityController as ctrl"
                        },
                        'rightnav': {
                            templateUrl: 'app/components/student/templates/rightnav.html'
                        }
                    }
                })
                .state('student.bragtags', {
                    url: "/kudos-nudges",
                    data:{
                        "isAuthenticated": true,
                        "authRedirect": "auth.login" 
                    },
                    stateParams: {
                        "dashboardTitle": "Kudos / Nudges"
                    },
                    views: {
                        'body@student': {
                            templateUrl: 'app/components/student/templates/bragtags.html',
                            controller: "studentBragtagController as ctrl"
                        }
                    }
                });
        }])
})();
