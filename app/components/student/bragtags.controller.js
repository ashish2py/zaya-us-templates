(function(){
	angular.module('zayaos.student')
		.controller('studentBragtagController',[ 'bragtags', function(bragtags){
			var ctrl = this;

			ctrl.posts = [];
			bragtags.getPosts().then(function(posts){
				console.log(posts);
				ctrl.posts = posts;
			}, function(error){
				console.log(error);
			});
		}]);
})();
