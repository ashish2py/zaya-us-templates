(function(){
	angular.module('zayaos.student')
		.controller('studentLessonController',['$stateParams','content','authentication', function($stateParams, content, authentication){
			var ctrl = this;
			ctrl.lesson = {};

			content.getNodeDetail($stateParams.lessonId).then(function(data){
				ctrl.lesson = data;
				// console.log(data);
			}, function(error){
				console.log(error);
			})


			ctrl.setLessonActive = function(indexActive)
			{	
				ctrl.activeIndex = indexActive;
			}
			ctrl.setLessonActive(0);

			ctrl.assessmentSubmit = function(){
				var lessonId = $stateParams.lessonId;
				getLessonSuggestion({
					"score": 0,
					"totalScore": 100,
					"playlistIndex": selectedPlaylist,
					"sr": lessonId
				})
				var userPlaylistdata = {
					"coursePlaylist": JSON.parse(localStorage.coursePlaylist),
					"roadMapData": JSON.parse(localStorage.roadMapData),
					"lessonResultMapping": JSON.parse(localStorage.lessonResultMapping)

				}
				authentication.updateUserPlaylist(userPlaylistdata).then(function(data){

				}, function(error){
					console.log(error);
				})

			}

			ctrl.getVideoUrl = function(url){
				return 'http://metalearn-api.zaya.in'+url;
			}
		}]);
})();
