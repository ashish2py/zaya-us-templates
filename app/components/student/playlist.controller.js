(function() {
    angular.module('zayaos.student')
        .controller('studentPlaylistController', [ 'authentication', function(authentication) {
            var ctrl = this;
            ctrl.user = authentication.getUser();
            ctrl.lists = ["SEL"];
            ctrl.cards = ["Student demonstrates an awareness of cultural issues and a respect for human dignity and differences", 
            "Student demonstrates ability to manage emotions constructively", "Student has a sense of personal responsibility"];
        }]);
})();
