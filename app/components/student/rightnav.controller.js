(function() {
    angular.module('zayaos.student')
        .controller('studentRightnavController', [ 'bragtags', 'authentication',function(bragtags, authentication){
        	var ctrl = this;
            ctrl.bragtags = null;
            ctrl.posts = null;
            ctrl.userList = authentication.dummyUserList;
            ctrl.chatMessages = [{
                "type": "other",
                "message": "Hello",
            }, {
                "type": "self",
                "message": "Hi, how are you?",
            }, {
                "type": "other",
                "message": "Good, You a did a great job.",
            }, {
                "type": "self",
                "message": "Thanks!",
            }, ]

            ctrl.addChatMessage = function(message){
                ctrl.chatMessages.push({
                    "type": "self",
                    "message": message
                });
            }

        	bragtags.getBragTags().then(function(bragtagsCollection){
        		ctrl.bragtags = bragtagsCollection;
        	}, function(error){
        		console.log(error);
        	});

            bragtags.getPosts().then(function(posts){
                ctrl.posts = posts;
            }, function(error){ 
                console.log(error);
            });

        }]);
})();
