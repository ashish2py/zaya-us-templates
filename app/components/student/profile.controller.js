(function() {
    angular.module('zayaos.student')
        .controller('studentProfileController', ['authentication', '$state', function(authentication, $state) {
            const NEXT_STATE = "student.dashboard";
            var ctrl = this;
            ctrl.user = authentication.getUser();
            ctrl.dummyProfile = authentication.dummyProfile;
            ctrl.intrestsString = ctrl.dummyProfile.intrests.join(", ");

            ctrl.updateProfile = function() {
                //Update intrests
                authentication.updateProfile(ctrl.user.profile).then(function(data) {
                    var updatedInterests = ctrl.intrestsString.split(',');
                    ctrl.dummyProfile.intrests = []
                    for (var i = 0; i < updatedInterests.length; i++) {
                        ctrl.dummyProfile.intrests.push(updatedInterests[i].trim());
                    }
                    $state.go(NEXT_STATE);

                }, function(error) {
                    console.log(error);
                })
            }
        }]);
})();
