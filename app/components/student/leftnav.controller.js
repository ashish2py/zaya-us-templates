(function() {
    angular.module('zayaos.student')
        .controller('studentLeftnavController', ['$location', '$scope', 'content', function($location, $scope, content) {
            var ctrl = this;
            ctrl.courses = null;

            $scope.isActive = function(viewLocation) {
                return viewLocation === $location.path();
            };

            //UI Setup
            content.getCourseList().then(function(courses) {
                ctrl.courses = courses;
            }, function(error) {
                console.log(error);
            });
        }]);
})();
