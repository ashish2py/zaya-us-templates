(function(){
	angular.module('zayaos.auth')
		.controller('signupController', [ 'authentication', '$state', function(authentication, $state){
			const NEXT_STATE = "student.dashboard";
			
			var ctrl = this;
			ctrl.user = {};
			ctrl.loginType = "student";

			ctrl.signUp = function(){
				authentication.register(ctrl.user).then(function(data){
					var next = NEXT_STATE;
					switch(ctrl.loginType){
						case "student":
							next = "student.dashboard";
							break;
						case "teacher":
							next = "teacher.dashboard";
							break;
						default:
							break;
					}
					$state.go(next);
				}, function(error){
					console.log(error);
				});
			}
		}]);
})();