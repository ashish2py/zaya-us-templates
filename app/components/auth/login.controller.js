(function(){
	angular.module('zayaos.auth')
		.controller('loginController' ,[ 'authentication', "$state",  function(authentication, $state){
			const ERROR_INVALID_CREDENTIAL = "Invalid Credential";
			const NEXT_STATE = "student.dashboard";
			const NEXT_STATE_MAP = {
				"teacher": "teacher.dashboard",
				"parent": "parent.sms"
			}

			var ctrl = this;
			ctrl.user = {};
			ctrl.loginType = "student";

			ctrl.login = function(){
				authentication.authenticate(ctrl.user.username, ctrl.user.password)
					.then(function(user){
						var next = NEXT_STATE_MAP[ctrl.user.username]?NEXT_STATE_MAP[ctrl.user.username]:NEXT_STATE;
						$state.go(next);
					}, function(error){
						ctrl.error = ERROR_INVALID_CREDENTIAL;
					});
			}
		}]);
})();