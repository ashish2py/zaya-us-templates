(function() {
    angular.module('zayaos.auth')
        .config(['$stateProvider', function($stateProvider) {
            // Login
            $stateProvider.state('auth', {
                    abstract: "true",
                    templateUrl: "app/components/auth/templates/auth.html"
                })
                .state('auth.login', {
                    url: "/login",
                    parent: "auth",
                    views: {
                        "": {
                            templateUrl: "app/components/auth/templates/auth.html"
                        },
                        "body": {
                            templateUrl: "app/components/auth/templates/auth.login.html",
                            controller: "loginController as ctrl"
                        }
                    }
                })
                .state('auth.signup',{
                    url: "/signup",
                    parent: "auth",
                    views: {
                        "": {
                            templateUrl: "app/components/auth/templates/auth.html"
                        },
                        "body": {
                            templateUrl: "app/components/auth/templates/auth.signup.html",
                            controller: "signupController as ctrl"
                        }
                    }  
                });
        }])
})();
