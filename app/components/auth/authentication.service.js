(function() {
    angular.module('zayaos.auth')
        .factory('authentication', ['$state', '$sessionStorage', 'apiConstant', '$q', "Restangular", function($state, $sessionStorage, apiConstant, $q, Restangular) {
            const SESSION_KEY_USER = "user";

            //Configure rest angular
            var restAuthApi = Restangular.withConfig(function(Configurer) {
                var authBaseUrl = apiConstant.serverUrl + apiConstant.apiAuthBase;
                Configurer.setBaseUrl(authBaseUrl);
            });
            var profileApi = Restangular.withConfig(function(Configurer) {
                var authBaseUrl = apiConstant.serverUrl + apiConstant.apiBase;
                Configurer.setBaseUrl(authBaseUrl);
            }).all(apiConstant.apiProfile);
            var loginApi = restAuthApi.all(apiConstant.apiLogin);
            var logoutApi = restAuthApi.all(apiConstant.apiLogout);
            var registarApi = restAuthApi.all(apiConstant.apiRegistration);
            var userApi = restAuthApi.all(apiConstant.apiUser);

            //Utils
            var createProfile = function(token, profiledata) {
                var header = {
                    "Authorization": "Token " + token
                }
                return profileApi.post(profiledata, {}, header)
                    .then(function(data) {
                        return data;
                    });
            }

            //Session utils
            var saveUserDetails = function(data) {
                var user = {
                    "id": data.id,
                    "token": data.token,
                    "username": data.username,
                    "first_name": data.first_name,
                    "last_name": data.last_name,
                    "profile": data.profile,
                    "phonenumber": data.phonenumber,
                    "email": data.email,
                    "token": data.token
                }
                $sessionStorage[SESSION_KEY_USER] = user;
                return $sessionStorage[SESSION_KEY_USER];
            }

            var getSaveUserDetails = function(token) {
                var header = {
                    "Authorization": "Token " + token
                }
                return userApi.customGET("", {}, header).then(function(response) {
                    return saveUserDetails(response.data);
                });

            }
            var deleteUserDetails = function() {
                delete $sessionStorage[SESSION_KEY_USER];
            }

            //Service interface
            var isAuthenticated = function() {
                return $sessionStorage[SESSION_KEY_USER] != null;
            }

            var authenticate = function(username, password) {
                var logindata = {
                    "username": username,
                    "password": password
                }
                return loginApi.post(logindata)
                .then(function(data) {
                    return Promise.resolve(getSaveUserDetails(data.data.key));
                })
                .then(function(userdata){
                    return getUserPlaylist().then(function(playlistdata){
                            localStorage.setItem("coursePlaylist", JSON.stringify(playlistdata.coursePlaylist));
                            return userdata;    
                        })
                }).catch(function(error){
                    console.error(error);
                });
                        
            }

            var getUser = function() {
                return $sessionStorage[SESSION_KEY_USER];
            }

            var register = function(user) {
                //User data
                var userdata = {
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "username": user.first_name,
                    "password1": user.password,
                    "password2": user.password,
                }
                if (user.email) {
                    userdata["email"] = user.email;
                }
                if (user.phonenumber) {
                    userdata["phonenumber"] = user.phonenumber;
                }

                //Profile data
                var profiledata = {
                    "first_name": user.first_name,
                    "last_name": user.last_name
                }
                if (user.dob) {
                    profiledata["dob"] = user.dob;
                }
                if (user.gender) {
                    profiledata["gender"] = user.gender;
                }
                if (user.grade) {
                    profiledata["grade"] = user.grade;
                }
                return registarApi.post(userdata).then(function(response) {
                    var userResponse = userdata;
                    userResponse.token = response.data.key;

                    return createProfile(response.data.key, profiledata).then(function(profileResponse) {
                        userResponse.profile = profileResponse.data;
                        getLessonSuggestion();
                        var userPlaylistdata = {
                            "coursePlaylist": JSON.parse(localStorage.coursePlaylist),
                            "roadMapData": JSON.parse(localStorage.roadMapData),
                            "lessonResultMapping": JSON.parse(localStorage.lessonResultMapping)

                        }
                        var user = {
                            "id": userResponse.id,
                            "token": userResponse.token,
                            "username": userResponse.username,
                            "first_name": userResponse.first_name,
                            "last_name": userResponse.last_name,
                            "profile": userResponse.profile,
                            "phonenumber": userResponse.phonenumber,
                            "email": userResponse.email,
                            "token": userResponse.token
                        }
                        $sessionStorage[SESSION_KEY_USER] = user;
                        updateUserPlaylist(userPlaylistdata).then(function(data){
                            return saveUserDetails(userResponse);
                        }, function(error){
                            console.log(error);
                        })
                        
                    });
                });
            }

            var updateProfile = function(profiledata) {
                if (isAuthenticated()) {
                    var user = getUser();
                    var profileApi = Restangular.withConfig(function(Configurer) {
                        var authBaseUrl = apiConstant.serverUrl + apiConstant.apiBase + apiConstant.apiProfile;
                        var header = {
                            "Authorization": "Token " + user.token
                        }
                        Configurer.setBaseUrl(authBaseUrl);
                        Configurer.setDefaultHeaders(header);
                    });
                    var profile = profileApi.one(profiledata.id);
                    profile.fullname = profiledata.fullname;
                    profile.dob = profiledata.dob;
                    return profile.patch().then(function(response) {
                        user.profile = response.data;
                        user.profile.avatar = user.profile.avatar.replace('http://metalearn-api.zaya.in', '')
                        $sessionStorage[SESSION_KEY_USER] = user;
                        return response;
                    }, function(error) {
                        console.log(error);
                    });

                } else {
                    //If not authenticated return error promiss
                    return $q(function(resolve, reject) {
                        reject({ 'error': 'not authenticated' })
                    });
                }
            }

            var logout = function() {
                if (isAuthenticated()) {
                    //Logout and delete session
                    var header = {
                        "Authorization": "Token " + $sessionStorage[SESSION_KEY_USER].token
                    }
                    return logoutApi.post({}, {}, header)
                        .then(function(data) {
                            deleteUserDetails();
                        });
                } else {
                    //If not authenticated return error promiss
                    return $q(function(resolve, reject) {
                        reject({ 'error': 'not authenticated' })
                    });
                }
            }


            var getUserPlaylist = function() {
                if (isAuthenticated()) {
                    //Logout and delete session
                    var header = {
                        "Authorization": "Token " + $sessionStorage[SESSION_KEY_USER].token
                    }
                    var playlistApi = Restangular.withConfig(function(Configurer) {
                        var authBaseUrl = apiConstant.serverUrl + apiConstant.apiBase;
                        Configurer.setBaseUrl(authBaseUrl);
                        Configurer.setDefaultHeaders(header);
                    }).all(apiConstant.apiPlaylist);
                    return playlistApi.getList().then(function(response){
                        return response[0].playlistdata;
                    });

                } else {
                    //If not authenticated return error promiss
                    return $q(function(resolve, reject) {
                        reject({ 'error': 'not authenticated' })
                    });
                }
            }

            var updateUserPlaylist = function(playlistdata) {
                if (isAuthenticated()) {
                    //Logout and delete session
                    var header = {
                        "Authorization": "Token " + $sessionStorage[SESSION_KEY_USER].token
                    }
                    var playlistApi = Restangular.withConfig(function(Configurer) {
                        var authBaseUrl = apiConstant.serverUrl + apiConstant.apiBase;
                        Configurer.setBaseUrl(authBaseUrl);
                        Configurer.setDefaultHeaders(header);
                    }).all(apiConstant.apiPlaylist);
                    return playlistApi.post(playlistdata);

                } else {
                    //If not authenticated return error promiss
                    return $q(function(resolve, reject) {
                        reject({ 'error': 'not authenticated' })
                    });
                }
            }

            var dummyProfile = {
                "about": "I am from Houston Texas, I like reading books, good music and nature",
                "address": "2264 Sundown Lane, Houston Texas",
                "phonenumber": "+1 999-999-9999",
                "intrests": [
                    "music", "baseball", "reading", "nature"
                ]
            }

            var dummyUserList = [{
                "id": "64335df2-866e-49dd-9dff-8b1dfa31bd31",
                "accounts": {
                    "testtest2": {
                        "id": "c65c1607-51c7-4d59-99ce-bdb066e43622",
                        "role": "account_owner",
                        "status": {
                            "number": 3,
                            "title": "NO_STATUS"
                        }
                    }
                },
                "root_node": {
                    "title": "testtest2_root_node",
                    "created_at": "2017-03-16T10:01:02.524155Z",
                    "id": "6888612d-78b8-441e-a639-2e5d01aad595"
                },
                "root_group": {
                    "title": "testtest2_root_group",
                    "created_at": "2017-03-16T10:01:02.549190Z",
                    "id": "f97b0687-b14c-42ab-8bbf-fe2b842c0fb3"
                },
                "username": "testtest2",
                "first_name": "testtest2",
                "last_name": "testtest2",
                "avatar": "app/assets/img/carlos-2.jpg",
                "deleted": null,
                "referral_code": "2789F083",
                "fullname": "Carlos Herrera",
                "gender": "M",
                "dob": null,
                "grade": 8,
                "profile_id": "69194904-3b09-4c97-b311-f7a104692a38",
                "client_uid": null,
                "user": "70c5a707-e028-4f0b-9eb3-9678f87a51a9"
            }, {
                "id": "64335df2-866e-49dd-9dff-8b1dfa31bd31",
                "accounts": {
                    "testtest2": {
                        "id": "c65c1607-51c7-4d59-99ce-bdb066e43622",
                        "role": "account_owner",
                        "status": {
                            "number": 3,
                            "title": "NO_STATUS"
                        }
                    }
                },
                "root_node": {
                    "title": "testtest2_root_node",
                    "created_at": "2017-03-16T10:01:02.524155Z",
                    "id": "6888612d-78b8-441e-a639-2e5d01aad595"
                },
                "root_group": {
                    "title": "testtest2_root_group",
                    "created_at": "2017-03-16T10:01:02.549190Z",
                    "id": "f97b0687-b14c-42ab-8bbf-fe2b842c0fb3"
                },
                "username": "testtest2",
                "first_name": "testtest2",
                "last_name": "testtest2",
                "avatar": "app/assets/img/jamal-2.jpg",
                "deleted": null,
                "referral_code": "2789F083",
                "fullname": "Jamal Barnes",
                "gender": "M",
                "profile_id": "59b84335-4471-47e6-8e11-171cab004c34",
                "dob": null,
                "grade": 8,
                "client_uid": null,
                "user": "70c5a707-e028-4f0b-9eb3-9678f87a51a9"
            },{
                "id": "64335df2-866e-49dd-9dff-8b1dfa31bd31",
                "accounts": {
                    "testtest2": {
                        "id": "c65c1607-51c7-4d59-99ce-bdb066e43622",
                        "role": "account_owner",
                        "status": {
                            "number": 3,
                            "title": "NO_STATUS"
                        }
                    }
                },
                "root_node": {
                    "title": "testtest2_root_node",
                    "created_at": "2017-03-16T10:01:02.524155Z",
                    "id": "6888612d-78b8-441e-a639-2e5d01aad595"
                },
                "root_group": {
                    "title": "testtest2_root_group",
                    "created_at": "2017-03-16T10:01:02.549190Z",
                    "id": "f97b0687-b14c-42ab-8bbf-fe2b842c0fb3"
                },
                "username": "testtest2",
                "first_name": "testtest2",
                "last_name": "testtest2",
                "avatar": "app/assets/img/amy-2.jpg",
                "deleted": null,
                "referral_code": "2789F083",
                "fullname": "Amy Chang",
                "profile_id": "63f80f24-f335-46db-a23f-fbf45b90753c",
                "gender": "M",
                "dob": null,
                "grade": 8,
                "client_uid": null,
                "user": "70c5a707-e028-4f0b-9eb3-9678f87a51a9"
            }, {
                "id": "64335df2-866e-49dd-9dff-8b1dfa31bd31",
                "accounts": {
                    "testtest2": {
                        "id": "c65c1607-51c7-4d59-99ce-bdb066e43622",
                        "role": "account_owner",
                        "status": {
                            "number": 3,
                            "title": "NO_STATUS"
                        }
                    }
                },
                "root_node": {
                    "title": "testtest2_root_node",
                    "created_at": "2017-03-16T10:01:02.524155Z",
                    "id": "6888612d-78b8-441e-a639-2e5d01aad595"
                },
                "root_group": {
                    "title": "testtest2_root_group",
                    "created_at": "2017-03-16T10:01:02.549190Z",
                    "id": "f97b0687-b14c-42ab-8bbf-fe2b842c0fb3"
                },
                "username": "testtest2",
                "first_name": "testtest2",
                "last_name": "testtest2",
                "avatar": "app/assets/img/alexa-2.jpg",
                "deleted": null,
                "referral_code": "2789F083",
                "fullname": "Alexa Jackson",
                "profile_id": "1cc29fe7-18c5-477e-a376-7286d470bb53",
                "gender": "M",
                "dob": null,
                "grade": 8,
                "client_uid": null,
                "user": "70c5a707-e028-4f0b-9eb3-9678f87a51a9"
            }]

            return {
                "isAuthenticated": isAuthenticated,
                "authenticate": authenticate,
                "logout": logout,
                "getUser": getUser,
                "register": register,
                "updateProfile": updateProfile,
                "getRole": null,
                "dummyProfile": dummyProfile,
                "dummyUserList": dummyUserList,
                "getUserPlaylist": getUserPlaylist,
                "updateUserPlaylist": updateUserPlaylist

            }
        }])
})();
