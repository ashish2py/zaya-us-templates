(function() {
    angular.module('zayaos.common')
        .factory('bragtags', ['Restangular', 'apiConstant', 'authentication', function(Restangular, apiConstant, authentication) {
            //User details
            var user = authentication.getUser();
            var profile_id = user.profile.id;
            var account_id = null;
            angular.forEach(user.profile.accounts, function(value, key) {
                    if (value.role == "account_owner") {
                        account_id = value.id;
                    }
                })
                //Configure rest angular
            var profileApi = Restangular.withConfig(function(Configurer) {
                var profileBaseUrl = apiConstant.serverUrl + apiConstant.apiBase + apiConstant.apiProfile;
                var header = {
                    "Authorization": "Token " + user.token
                };
                Configurer.setBaseUrl(profileBaseUrl);
                Configurer.setDefaultHeaders(header);
            }).one(profile_id);
            var accountApi = Restangular.withConfig(function(Configurer) {
                var accountBaseUrl = apiConstant.serverUrl + apiConstant.apiBase + apiConstant.apiAccount;
                var header = {
                    "Authorization": "Token " + user.token
                };
                Configurer.setBaseUrl(accountBaseUrl);
                Configurer.setDefaultHeaders(header);
            }).one(account_id);
            var bragtagsApi = accountApi.all(apiConstant.apiBragtag);
            var postsApi = profileApi.all(apiConstant.apiPost);
            var bragtagCollectionApi = accountApi.all(apiConstant.apiBragtagPack);

            //Util func
            var getFormData = function(object) {
                var formdata = new FormData();
                angular.forEach(object, function(value, key) {
                    if (value instanceof Array) {
                        //Convert array to comman seprated values
                        var string = "";
                        for (var i = 0; i < value.length; i++) {
                            if (string == "") {
                                string = value[i];
                                continue;
                            }
                            string += "," + value[i];
                        }
                        formdata.append(key, string);
                    } else {
                        formdata.append(key, value);
                    }
                })
                return formdata;
            }

            // Utils
            var getBragTags = function() {
                return bragtagsApi.getList();
            }
            var createBragTags = function(bragtag) {
                var formdata = getFormData({
                    'title': bragtag.title,
                    "description": bragtag.description,
                    'image': bragtag.image
                })
                return bragtagsApi
                    .withHttpConfig({ transformRequest: angular.identity })
                    .customPOST(formdata, '', undefined, { 'Content-Type': undefined });
            }

            var deleteBragTags = function(bragtag) {
                return bragtagsApi.one(bragtag.id).remove();
            }

            var getPosts = function() {
                return postsApi.getList({'ver':'web'});
            }

            var createPost = function(post) {
                var formdata = getFormData({
                    "title": post.title,
                    "bragtags": post.bragtags,
                    "tag_to": post.tag_to
                });
                if (post.image){
                    formdata.append("image", post.image);                    
                }
                console.log(formdata);
                return postsApi
                    .withHttpConfig({ transformRequest: angular.identity })
                    .customPOST(formdata, '', undefined, { 'Content-Type': undefined });
            }

            var getBragtagCollections = function() {
                return bragtagCollectionApi.getList();
            }

            var createBragtagCollection = function(bragCollection) {
                var data = {
                    "title": bragCollection.title,
                    "bragtags": bragCollection.bragtags,
                }
                return bragtagCollectionApi.post(data).then(function(data) {
                    return data.data;
                });
            }

            var getBragtagCollectionDetail = function(collectionId) {
                return bragtagCollectionApi.get(collectionId).then(function(response) {
                    return response.data;
                });
            }

            var updateBragTagCollection = function(collectionId, data) {
                var collection = bragtagCollectionApi.one(collectionId);
                collection.title = data.title;
                collection.description = data.description;
                collection.bragtags = data.bragtags;
                return collection.patch();
            }

            var getPostsForId = function(profileId) {
                var user = authentication.getUser();
                var profileApi = Restangular.withConfig(function(Configurer) {
                    var profileBaseUrl = apiConstant.serverUrl + apiConstant.apiBase + apiConstant.apiProfile;
                    var header = {
                        "Authorization": "Token " + user.token
                    };
                    Configurer.setBaseUrl(profileBaseUrl);
                    Configurer.setDefaultHeaders(header);
                }).one(profileId);
                var postsApi = profileApi.all(apiConstant.apiPost);
                return postsApi.getList();
            }

            return {
                "getPosts": getPosts,
                "getPostsForId": getPostsForId,
                "createPost": createPost,

                "getBragTags": getBragTags,
                "createBragTags": createBragTags,
                "deleteBragTags": deleteBragTags,

                "getBragtagCollections": getBragtagCollections,
                "getBragtagCollectionDetail": getBragtagCollectionDetail,
                "createBragtagCollection": createBragtagCollection,
                "updateBragTagCollection": updateBragTagCollection
            }
        }])
})();
