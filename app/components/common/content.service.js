(function() {
    angular.module('zayaos.common')
        .factory('content', ['Restangular', 'apiConstant', 'authentication', function(Restangular, apiConstant, authentication) {
            //User details
            var user = authentication.getUser();
            var apiBase = Restangular.withConfig(function(Configurer) {
                var apiBaseUrl = apiConstant.serverUrl + apiConstant.apiBase;
                var header = {
                    "Authorization": "Token " + user.token
                };
                Configurer.setBaseUrl(apiBaseUrl);
                Configurer.setDefaultHeaders(header);
            });
            var courseApi = apiBase.all(apiConstant.apiCourses);
            var nodeApi = apiBase.all(apiConstant.apiNode);

            var getCourseApi = function() {
                var user = authentication.getUser();
                var apiBase = Restangular.withConfig(function(Configurer) {
                    var apiBaseUrl = apiConstant.serverUrl + apiConstant.apiBase;
                    var header = {
                        "Authorization": "Token " + user.token
                    };
                    Configurer.setBaseUrl(apiBaseUrl);
                    Configurer.setDefaultHeaders(header);
                });
                return apiBase.all(apiConstant.apiCourses);
            }

            var getNodeApi = function() {

                var user = authentication.getUser();
                var apiBase = Restangular.withConfig(function(Configurer) {
                    var apiBaseUrl = apiConstant.serverUrl + apiConstant.apiBase;
                    var header = {
                        "Authorization": "Token " + user.token
                    };
                    Configurer.setBaseUrl(apiBaseUrl);
                    Configurer.setDefaultHeaders(header);
                });
                return apiBase.all(apiConstant.apiNode);

            }

            var getCourseList = function() {
                return getCourseApi().getList();
            }

            var getNodeList = function(idLists) {
                var idString = "";
                for (var i = 0; i < idLists.length; i++) {
                    if (i == 0) {
                        idString = idLists[i];
                    } else {
                        idString += "," + idLists[i];
                    }
                }
                var params = {
                    "id": idString,
                    "limit": idLists.length
                }
                return getNodeApi().getList(params);
            }

            var getNodeDetail = function(nodeId) {
                return nodeApi.get(nodeId).then(function(response) {
                    return response.data;
                });
            }

            return {
                "getCourseList": getCourseList,
                "getNodeList": getNodeList,
                "getNodeDetail": getNodeDetail,
            }
        }]);
})();
