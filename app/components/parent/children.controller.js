(function() {
    angular.module('zayaos.parent')
        .controller('parentChildrenController', [ "$scope", "$location", '$timeout', function($scope, $location, $timeout){
        	var ctrl = this;
                        ctrl.barChart = {
                xkey: 'skill',
                ykeys: ['class_avg','your'],
                labels: ['Class Avg.','Your Score'],
                barColors: ['#999','#4285f4'],
                barRatio: 0.4,
                xLabelAngle: 35,
                hideHover: 'auto'
            }

            $scope.students = [{
                    name:"Alexa Jackson",
                    subtitle:'In Conversation',
                    pic:"/app/assets/img/alexa-2.jpg"
                }
            ]

            $scope.tabs = [{
                percent:30,
                icon:'english.png',
                barChart: $scope.barChart1,
                subject:"ELA",
                goals:[{
                    title:"ELA",
                    steps:[ "I will read one extra book outside of class",
                            "I will master the lesson on inferences",
                            "I will practice making inferences with my parents at home"
                        ]
                }],
                objectives:[{
                    title:"Objective 1",
                    progress:10,
                    total:10,
                    description:"Demonstrate command of the conventions of standard English, capitalization, punctuation and spelling when writing"
                },
                {
                    title:"Objective 2",
                    progress:5,
                    total:10,
                    description:"Demonstrate command of the conventions of standard English grammar and usage when writing"
                },
                {
                    title:"Objective 3",
                    progress:15,
                    total:20,
                    description:" Demonstrate understanding of figurative language, word relationships, and nuances in word meaning"
                },
                {
                    title:"Objective 4",
                    progress:2,
                    total:8,
                    description:"Cite textual evidence to support analysis of what the text says explicityly as well as inferences drawn from the text"

                }],
                focusAreas:[
                    {
                        title:"Conventions",
                        progress:30
                    },
                    {
                        title:"Grammar",
                        progress:36
                    },
                    {
                        title:"Reading Comprehension",
                        progress:30
                    },
                    {
                        title:"Vocabulary",
                        progress:60
                    },
                    {
                        title:"Writing",
                        progress:30
                    },
                    {
                        title:"Speaking and Listening",
                        progress:100
                    }
                ],
                barChartData: [
                        {skill: 'relationships vocabulary', your: 3, class_avg: 10},
                        {skill: 'positive feedback skills', your: 1, class_avg: 9},
                        {skill: 'Listening skills', your: 3, class_avg: 8},
                        {skill: 'Collaborative Skills', your: 5, class_avg: 6},
                        {skill: 'Attribute identification', your: 2, class_avg: 9},
                        {skill: 'Discern behaviors', your: 4, class_avg: 10}
                      ]
            },{
                percent:60,
                icon:'maths.png',
                subject:"Math",
                goals:[{
                    title:"Math",
                    steps:[ "I will master subtracting fractions this month",
                            "My math homework will be submitted on time each week",
                            "I will help a friend learn his fractions"
                        ]
                    }],
                objectives:[{
                    title:'Objective 1',
                    progress:1,
                    total:9,
                    description:'Understand the concept of fractions',
                },{
                    title:'Objective 2',
                    progress:2,
                    total:5,
                    description:'Add Fractions',
                },{
                    title:'Objective 3',
                    progress:1,
                    total:10,
                    description:'Subtract Fractions',
                },{
                    title:'Objective 4',
                    progress:3,
                    total:6,
                    description:'Multiply and Divide Fractions'

                }],
                focusAreas:[
                    {
                        title:"Operations and Algebraic thinking",
                        progress:30
                    },{
                        title:"Operations in Base 10",
                        progress:60
                    },{
                        title:"Operations - Fractions",
                        progress:76
                    },{
                        title:"Measurement and Data",
                        progress:90
                    },{
                        title:"Geometry",
                        progress:45
                    }],
                barChartData: [
                        {skill: 'same denominator', your: 3, class_avg: 10},
                        {skill: 'different denominator', your: 9, class_avg: 9},
                        {skill: 'mixed numbers - same denominator', your: 6, class_avg: 8},
                        {skill: 'mixed numbers - different denominator', your: 5, class_avg: 6},
                        {skill: 'line-plot problem -fractions', your: 7, class_avg: 9},
                        {skill: 'word problem -fractions', your: 6, class_avg: 10}
                      ]
                
            },{
                percent:50,
                icon:'hnw.png',
                subject:"Health and Wellness",
                goals:[{
                    title:"Health and Wellness",
                    steps:[ "I will record what I eat this week",
                            "Our team will win our baseball game in Saturday",
                            "I will increse my heart rate by 10% this week during running"
                        ]
                    }],
                objectives:[{
                        title:"Standard 1",
                        progress:3,
                        total:6,
                        description:"Student exhibits a health-enhancing physically active lifestyle that provides enjoyment and challenges"
                    },
                    {
                        title:"Standard 2",
                        progress:1,
                        total:8,
                        description:"Student understands the benefits from involvement in daily physical activity"
                    },
                    {
                        title:"Standard 3",
                        progress:2,
                        total:8,
                        description:"Student understands the relationship between optimal body function and healthy eating"
                    },
                    {
                        title:"Standard 4",
                        progress:4,
                        total:4,
                        description:"Student understands the practice of mindfulness and its benefits"
                    }],
                focusAreas:[
                    {
                        title:"Daily Physical Activity",
                        progress:10
                    },
                    {
                        title:"Healthy Eating",
                        progress:40
                    },
                    {
                        title:"Sports and Games",
                        progress:60
                    },
                    {
                        title:"Heart Rate Monitoring",
                        progress:30
                    },
                    {
                        title:"Strength and conditioning",
                        progress:90
                    },
                    {
                        title:"Mindfulness",
                        progress:90
                    }
                ],
                barChartData: [
                        {skill: 'verb tense', your: 5, class_avg: 10},
                        {skill: 'punctuation in series', your: 2, class_avg: 9},
                        {skill: 'correct spelling', your: 8, class_avg: 8},
                        {skill: 'suffixes', your: 5, class_avg: 6},
                        {skill: 'draw inferences', your: 3, class_avg: 9},
                        {skill: 'summarize text', your: 7, class_avg: 10}
                      ]

            },{
                percent:100,
                icon:'sel.png',
                barChart: $scope.barChart1,
                subject:"SEL",
                goals:[{
                    title:"SEL",
                    steps:[ "I will make 2 new friends this month",
                            "I will give 1 Kudo to a friend each day",
                            "I will receive a Kudo for how well I collaborate on a project this month"
                        ]
                    }],
                objectives:[{
                    title:"Goal IV",
                    progress:1,
                    total:5,
                    description:"Demonstrate interpersonal skills needed to establish and maintain positive relationships"
                },
                {
                    title:"Objective A", 
                    progress:2,
                    total:12,
                    description:"Student uses positive communication and social skills"
                },
                {
                    title:"Objective 3",
                    progress:11,
                    total:15,
                    description:"Student develops constructive relationships"
                    
                },
                {
                    title:"Objective 4",
                    progress:14,
                    total:15,
                    description:"Student demonstrates an ability to prevent, manage, and resolve interpersonal conflicts in constructive ways"
                }],
                focusAreas:[
                    {
                        title:"Giving positive feedback",
                        progress:100
                    },
                    {
                        title:"Listening",
                        progress:30
                    },
                    {
                        title:"Collaborating",
                        progress:60
                    },
                    {
                        title:"Discerning",
                        progress:80
                    },
                    {
                        title:"Constructive problem solving",
                        progress:90
                    }
                ],
                barChartData: [
                        {skill: 'daily physical activity', your: 10, class_avg: 10},
                        {skill: 'sports', your: 10, class_avg: 9},
                        {skill: 'exercise benefits', your: 9, class_avg: 8},
                        {skill: 'heart rate', your: 10, class_avg: 6},
                        {skill: 'healthy eating', your: 10, class_avg: 9},
                        {skill: 'mindfulness', your: 10, class_avg: 10}
                      ]

            }]

            ctrl.loadRandomTab = function() {
                ctrl.loadTab(Math.floor(Math.random() * (($scope.tabs.length-1) - 0 + 1)));
            }

            ctrl.loadTab = function(tab)
            {

                console.log('clicked', tab);
                $timeout(function(){
                    console.log('eggzecuted');
                    if ($scope.isActive==undefined || $scope.isActive != tab)
                    {
                        $scope.isActive = tab;
                        $scope.tabContent  = $scope.tabs[$scope.isActive];
                    }
                }, 100);

            }

        }]);
})();
 