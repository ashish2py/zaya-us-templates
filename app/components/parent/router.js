(function() {
    angular.module('zayaos.parent')
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider.state('parent', {
                    url: "/parent",
                    abstract: "true",
                    views: {
                        '': {
                            templateUrl: 'app/components/parent/templates/base.html',
                        },
                        'header@parent': {
                            templateUrl: 'app/components/parent/templates/header.html',
                            controller: "parentHeaderController as ctrl"
                        },
                        'leftnav@parent': {
                            templateUrl: 'app/components/parent/templates/leftnav.html',
                            controller: "parentLeftnavController as ctrl"
                        }
                    }
                })
                .state('parent.sms', {
                    url: "/sms",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Messages"
                    },
                    views: {
                        'body@parent': {
                            templateUrl: 'app/components/parent/templates/sms.html',
                        }
                    }
                })

                .state('parent.smsdetail', {
                    url: "/smsdetail",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Messages"
                    },
                    views: {
                        'body@parent': {
                            templateUrl: 'app/components/parent/templates/sms_details.html',
                        }
                    }
                })

                .state('parent.schedule', {
                    url: "/schedule",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Schedule"
                    },
                    views: {
                        'body@parent': {
                            templateUrl: 'app/components/parent/templates/schedule.html',
                            controller: "parentScheduleController as ctrl"
                        }
                    }
                })

                .state('parent.handbook', {
                    url: "/handbook",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Handbook"
                    },
                    views: {
                        'body@parent': {
                            templateUrl: 'app/components/parent/templates/handbook.html',
                        }
                    }
                })
                .state('parent.childrens', {
                    url: "/children",
                    data: {
                        "isAuthenticated": true,
                        "authRedirect": "auth.login"
                    },
                    stateParams: {
                        "dashboardTitle": "Children"
                    },
                    views: {
                        'body@parent': {
                            templateUrl: 'app/components/parent/templates/classes.html',
                            controller: "parentChildrenController as ctrl"
                        }
                    }
                });
        }])
})();
