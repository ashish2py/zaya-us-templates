(function() {
    angular.module('zayaos.parent')
        .controller('parentLeftnavController', [ "$scope", "$location", function($scope, $location){
        	$scope.isActive = function (viewLocation) { 
                return viewLocation === $location.path();
            };
        }]);
})();
 