(function() {
    angular.module('zayaos', ['ui.router', 'ui.bootstrap', 'ui.calendar','ngSanitize', 'ngCountUp', 'sparkline', 'perfect_scrollbar', 'angular-flot', 'angular.morris', 'ui.sortable', 'chart.js', 'restangular', 'ngStorage', 'zayaos.constant','zayaos.auth', 'zayaos.student', 'zayaos.teacher', 'zayaos.common', 'zayaos.parent']);
})();
