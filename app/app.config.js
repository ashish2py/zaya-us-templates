(function() {
    angular.module('zayaos')
        .run(function($rootScope, $state, authentication) {
            //Memorize state to redirect after successful login
            var self = this;
            self.authentication = authentication;
            self.$state = $state;
            self.authFailedState = null;

            $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {
                if (toState.data && toState.data.isAuthenticated) {
                    if (!self.authentication.isAuthenticated()) {
                        if (toState.data.authRedirect) {
                            self.authFailedState = toState.name;
                            self.$state.go(toState.data.authRedirect);
                        }
                    } else {
                        //Redirect to page trying to access
                        if (self.authFailedState) {
                            self.$state.go(authFailedState);
                            self.authFailedState = null;
                        }
                    }
                }
            });
        })
        .config(['RestangularProvider', '$sceDelegateProvider' ,function(RestangularProvider, $sceDelegateProvider) {

            //Append slash to each request
            RestangularProvider.setRequestSuffix("/");
            
            //Change response to support pagination
            RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
                var newResponse;
                if (operation === "getList") {
                    newResponse = data.results;
                    newResponse.metadata = {
                        "count": data.count,
                        "next": data.next,
                        "previous": data.previous
                    }
                } else {
                    newResponse = response;
                }
                return newResponse;
            });

            $sceDelegateProvider.resourceUrlWhitelist([
                'self',
                'https://www.youtube.com/**'
              ]);

        }]);
})();
