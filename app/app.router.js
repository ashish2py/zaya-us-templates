(function() {
    angular.module('zayaos')
        .config(['$urlRouterProvider', '$injector', '$locationProvider',function($urlRouterProvider, $injector,  $locationProvider) {
            $urlRouterProvider.otherwise(function ($injector) {
                 $injector.get('$state').go('auth.login');
            });
	        $locationProvider.hashPrefix('');
        }])
})();
