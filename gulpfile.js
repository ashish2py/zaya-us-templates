
var gulp = require('gulp'),
    concat = require("gulp-concat"),
    uglify = require("gulp-uglify"),
    cleanCSS = require('gulp-clean-css'),
    browserSync = require('browser-sync').create();

var srcjs = [
    "./app/app.constant.js",
    "./app/components/**/module.js",
    "./app/components/**/*service*.js",
    "./app/components/**/*controller*.js",
    "./app/components/**/router.js",
    "./app/app.module.js",
    "./app/shared/*.js",
    "./app/app.config.js",
    "./app/app.router.js"
];

var vendorjs = [
    "./app/assets/lib/jquery/dist/jquery.min.js",
    "./app/assets/lib/jquery-ui/jquery-ui.min.js",
    "./app/assets/lib/d3/d3.js",
    "./app/assets/js/ml/ml-custom.js",
    "./app/assets/lib/Flot/jquery.flot.js",
    "./app/assets/lib/flot/jquery.flot.pie.js",
    "./app/assets/lib/flot/jquery.flot.resize.js",
    "./app/assets/lib/flot.curvedlines/curvedLines.js",
    "./app/assets/lib/angular/angular.min.js",
    "./app/assets/lib/angular-ui-router/release/*.min.js",
    "./app/assets/lib/angular-sanitize/angular-sanitize.min.js",
    "./app/assets/lib/lodash/lodash.js",
    "./app/assets/lib/restangular/dist/restangular.min.js",
    "./app/assets/lib/ngstorage/ngStorage.min.js",
    "./app/assets/lib/bootstrap/dist/js/bootstrap.min.js",
    "./app/assets/lib/angular-bootstrap/ui-bootstrap-tpls.min.js",
    "./app/assets/lib/angular-ui-sortable/sortable.min.js",
    "./app/assets/lib/chart.js/dist/Chart.min.js",
    "./app/assets/lib/angular-chart.js/dist/angular-chart.min.js",
    "./app/assets/lib/angular-flot/angular-flot.js",
    "./app/assets/js/jquery.sparkline/jquery.sparkline.min.js",
    "./app/assets/lib/countUp.js/countUp.min.js",
    "./app/assets/lib/ng-countUp.js/ngCountUp.min.js",
    "./app/assets/lib/raphael/raphael.min.js",
    "./app/assets/lib/morris.js/morris.min.js",
    "./app/assets/lib/angular-morris/build/module/angular-morris/angular-morris.js",
    "./app/assets/lib/perfect-scrollbar/min/perfect-scrollbar.min.js",
    "./app/assets/lib/angular-perfect-scrollbar/src/angular-perfect-scrollbar.js",
    "./app/assets/lib/moment/min/moment.min.js",
    "./app/assets/lib/fullcalendar/dist/fullcalendar.min.js",
    "./app/assets/lib/angular-ui-calendar/src/calendar.js",
    "./app/assets/lib/fullcalendar/dist/gcal.js",
]

var srccss = [
    "./app/assets/lib/perfect-scrollbar/min/perfect-scrollbar.min.css",
    "./app/assets/lib/fullcalendar/dist/fullcalendar.min.css",
    "./app/assets/css/material-design-icons/*.css",
    "./app/assets/css/custom/style.css",
    "./app/assets/css/custom/ml-custom.css",
    "./app/assets/css/custom/custom.css",
    "./app/assets/css/custom/flex.css",
    "./app/assets/css/custom/morris.css",
    "./app/assets/css/custom/verbose.css"
]

gulp.task('js', function() {
    gulp.src(srcjs)
        .pipe(concat('app.min.js'))
        // .pipe(uglify())
        .pipe(gulp.dest('./app/assets/js/'))
        .pipe(browserSync.stream());
});

gulp.task('css', function() {
    gulp.src(srccss)
        .pipe(concat('app.min.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./app/assets/css'))
        .pipe(browserSync.stream());
});

gulp.task('vendorjs', function() {
    gulp.src(vendorjs)
        .pipe(concat('vendor.min.js'))
        // .pipe(uglify())
        .pipe(gulp.dest('./app/assets/js/'))
        .pipe(browserSync.stream());
});

gulp.task('watch', function(){
    gulp.watch(srccss, ["css"]);
    gulp.watch(vendorjs, ["vendorjs"]);
    gulp.watch(srcjs, ["js"]);
});

gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch(srccss, ["css"]);
    gulp.watch(vendorjs, ["vendorjs"]);
    gulp.watch(srcjs, ["js"]);
    gulp.watch("./app/components/**/templates/*.html").on('change', browserSync.reload);
});

gulp.task('default', ['css', 'vendorjs', 'js']);

